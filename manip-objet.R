# Manip sur les objets:

# vecteurs
# list
# dataframe
# matrice...


# vecteurs ---------------------------------------------
c( 30, 40, 50)
"affichage / insertion en ligne"


# dataframe --------------------------------------------
df <- data.frame( c( 30, 40, 50), c( 110, 120, 130 ),c( 280, 285,290))
df
"affichage / insertion des c() en colonne"

names(df) <- c("c1", "c2", "c3")  
"pour définir le noms des 3 champs du df"

"df[ ligne, col ]"
df[,]
df[,2]  # la col=2 : toutes les lignes de la col =2
df[2,]  # la ligne=2 : toutes les col de la ligne =2
df[1,2] # l'élément à la ligne 1, col 2

"Extraction de plusieurs lignes. Ex: ligne 1 et 3 "
df[ c(1,3)  ,   ]
df[ c(1,3)  , 2  ]

"Extraction de plusieurs col. Ex: col 2 et 3 "
df[     , c(2,3) ]



# list --------------------------------------------
a <- list(b=3,c=4) # a est une liste qui contient b et c
print( a$b )       # la machine affiche la valeur 3 (contenu de b)

length(a)          # 2 car il y a deux �l�ments dans a
names(a)           # correspond aux noms dans a, soit le vecteur "b" "c"

# une �criture pratique :

res <- list(methode="simple",
            fichier="a35.xls",
            jour=28
) # fin de list

# on peut aussi commenter en partie droite

xmp <- list(methode="simple",      # ceci montre comment on
            fichier="a35.xls",     # peut avoir du code tr�s
            jour=28                # lisible pour une liste
) # fin de list

# des listes plus g�n�rales :

a <- 5
b <- list(a=a,c=8)
c <- 1:3
d <- list(a,b,c,d=3)

e <- list(x=1,y=c(2,3),z=list(a=4,b=5,c=6),t="bravo !")


